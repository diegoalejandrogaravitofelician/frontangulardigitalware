import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ProductoComponent } from './producto/producto.component';
import { InventarioComponent } from './inventario/inventario.component';
import { CompraComponent } from './compra/compra.component';
const routes: Routes =[
{path: '',component:HomeComponent},
{path: 'clientes',component:ClientesComponent},
{path: 'producto',component:ProductoComponent},
{path: 'inventario',component:InventarioComponent},
{path: 'compra',component:CompraComponent},
]

@NgModule({
 imports :[RouterModule.forRoot(routes),RouterModule.forChild(routes)],
 exports : [RouterModule]
})
export class AppRoutingModule { }
