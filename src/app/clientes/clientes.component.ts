import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../clientes.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  constructor(
    private ClientesService: ClientesService
  ) { }

  ngOnInit(): void {
    this.getClientes();
  }

  getClientes() {
    this.ClientesService.getClientes().subscribe((succ: any) => {
      console.log(succ);
    })
  }

}
