import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  setCliente: boolean = false;
  setProductos: boolean = false;
  setInventario: boolean = false;
  setCompra: boolean = false;

  Cont: any;
  constructor(
  ) { }

  ngOnInit(): void {
  }

  ValidarHome(id: any) {
    console.log(id)
    if(id == 1){
      window.location.href = "/clientes"
    }
    if(id == 2){
      window.location.href = "/producto"
    }
    if(id == 3){
      window.location.href = "/inventario"
    }
    if(id == 4){
      window.location.href = "/compra"
    }
  }

}
